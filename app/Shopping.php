<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shopping extends Model
{

    static function create($request){

        $insert  = New Shopping;
        $insert->createdate  = $request['createdate'];
        $insert->name  = $request['name'];

        $save = $insert->save();

        if($save){
            return array(
                "id"=>$insert->id,
                "name"=>$insert->name,
                "createdate"=>$insert->createdate
            );
        }
    }

    static function getAll($request){

        $result = Shopping::all();

        return $result;
    }

    static function getById($request){

        $result = Shopping::where('id', $request['id'])->first();

        return $result;
    }

    static function updateData($request){
        $shopping = $request->input('shopping');
        $update = Shopping::where('id', $request['id'])->firstOrFail();
        $update->createdate  = $shopping['createdate'];
        $update->name  = $shopping['name'];

        $save = $update->save();

        return $save;
    }

    static function deleteData($request){

        $result = DB::table('shoppings')->where('id', $request['id'])->delete();

        return $result;
    }
}
