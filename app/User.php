<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];


    function create($request){


        $insert  = New User;
        $insert->username  = $request['username'];
        $insert->email  = $request['email'];
        $insert->encrypted_password  = $request['encrypted_password'];
        $insert->phone  = $request['phone'];
        $insert->address  = $request['address'];
        $insert->city  = $request['city'];
        $insert->country  = $request['country'];
        $insert->name  = $request['name'];
        $insert->postcode  = $request['postcode'];

        $save = $insert->save();

        return $insert->username;
  }
}
