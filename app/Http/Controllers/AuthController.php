<?php

namespace App\Http\Controllers;

use Validator;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Create a new token.
     *
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60*60 // Expiration time
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  \App\User   $user
     * @return mixed
     */
    public function signup(User $user) {
        $data_user = $this->request->input('user');
        /*$this->validate($data_user, [
            'username'     => 'required',
            'encrypted_password'  => 'required'
        ]);*/

        $password = $data_user['encrypted_password'];
        $bpassword = sha1($password);
        $hash_pass = sha1($bpassword);

        // Find the user by username
        $user = User::where('email', $data_user['email'])->first();

		if($user){
			$user = $user;
		}

        if (!$user) {
	        if($data_user['username']) {
				$respon = app('db')->table('users')->insertGetId(array('username' => $data_user['username'], 'password'=>$hash_pass, 'email'=> $data_user['email'], 'phone'=> $data_user['username'], 'address'=> $data_user['address'], 'city'=> $data_user['city'], 'country'=> $data_user['country'], 'name'=> $data_user['username'], 'postcode'=>$data_user['postcode'] ));

				//$respon = User::create(array('username' => $data_user['username'], 'encrypted_password'=>$hash_pass, 'email'=> $data_user['email'], 'phone'=> $data_user['username'], 'address'=> $data_user['address'], 'city'=> $data_user['city'], 'country'=> $data_user['country'], 'name'=> $data_user['username'], 'postcode'=>$data_user['postcode'] ));

        	$payload =[
            	'iss' => 'jwt',
            	'sub' => 'register',
            	'iat' => time(),
            	'exp' => time() +60*60*24
        	];

			$token=JWT::encode($payload, env('JWT_SECRET'));

	            return response()->json([
					'username' => $data_user['username'],
					'token' => $token,
					'email' => $data_user['email']
       	    	 ],200);
       		 }
        }

        // Bad Request response
        return response()->json([
            'error' => 'Username sudah terdaftar.'
        ], 400);
    }

    public function signin(User $user)
    {
        $this->validate($this->request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('email', $this->request->input('email'))->first();
		if($user){
			$user = $user;
		}

        if(!$user){
            return response()->json([
                'error' => 'email tdk terdaftar'
            ], 400);
        }

        $password = sha1($this->request->input('password'));
        $hash_pass = sha1($password);

        if($hash_pass==$user->password) {
			$respon = array('token'=>$this->jwt($user),'data'=>$user);
            return response()->json([
				'email' => $user['email'],
                'token' => $this->jwt($user),
				'username' => $user['username'],
            ],200);
        }

        return response()->json([
			'msg' => 'failed',
            'content' => '',
			'success' => FALSE,
			'token_status' => ''
        ],400);
    }

}
