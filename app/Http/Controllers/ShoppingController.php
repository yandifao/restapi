<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Shopping;

class ShoppingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        //
        $this->request = $request;
    }

    public function create()
    {
        try {
            $result = Shopping::create(array($this->request));

            return response()->json([
                'data' => $result
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th
            ],400);
        }
    }

    public function getAll()
    {
        try {
            $result = Shopping::getAll($this->request);

            return response()->json([
                'data' => $result
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th
            ],400);
        }
    }

    public function getById()
    {
        try {
            $result = Shopping::getById($this->request);

            return response()->json([
                'data' => $result
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th
            ],400);
        }
    }

    public function update()
    {
        try {
            $result = Shopping::updateData($this->request);

            return response()->json([
                'data' => $result
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th
            ],400);
        }
    }

    public function delete()
    {
        $result = Shopping::deleteData($this->request);

        return response()->json([
            'data' => $result
        ],200);
        exit();
        try {
            $result = Shopping::deleteData($this->request);

            return response()->json([
                'data' => $result
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'error' => $th
            ],400);
        }
    }
}
