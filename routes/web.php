<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//user
$router->post('api/users/signup', ['uses' => 'AuthController@signup']);
$router->post('api/users/signin', ['uses' => 'AuthController@signin']);

$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
    $router->get('api/users', function() {
        $users = \App\User::all();
        return response()->json($users);
    });

    //shopping
    $router->post('api/shopping/create', ['uses' => 'ShoppingController@create']);
    $router->get('api/shopping', ['uses' => 'ShoppingController@getAll']);
    $router->get('api/shopping/{id}', ['uses' => 'ShoppingController@getById']);
    $router->put('api/shopping/{id}', ['uses' => 'ShoppingController@update']);
    $router->delete('api/shopping/{id}', ['uses' => 'ShoppingController@delete']);
});

